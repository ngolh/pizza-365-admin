# Pizza 365 ADMIN
- Đây là bản mô tả các chức năng trang Admin của website nhà hàng pizza 365.
- Bao gồm các chức năng chính như sau.

## 🌟 Các chức năng của Pizza 365 ADMIN'

### 1. hiển thị danh sách đơn hàng

#### 🧱 Danh sách đơn hàng này có các chức năng:

- Hiển thị chi tiết đơn hàng (Tên, Số điện thoại, Trạng thái đơn hàng,...)
- Nút <button style="background-color: green ; color:white" >thêm đơn hàng</button> để chuyển đến trang tạo một đơn hàng mới
- Nút <button style="background-color: darkblue ; color:white" >Chi tiết</button> để xem chi tiết đơn hàng
- Nút <button style="background-color: red ; color:white" >Delete</button> để xóa đơn hàng

![OrderList](imgMarkdown/orderList.PNG)

### 2. Chức năng tạo đơn hàng mới

#### ✨ Thao tác tạo đơn hàng mới:
- Sau khi bấm nút <button style="background-color: green ; color:white" >thêm đơn hàng</button> sẽ hiện lên giao diện sau

![CreateOrder](imgMarkdown/createOrder.PNG)

- Điền thông tin sau đó bấm <button style="background-color: darkblue ; color:white" >Create Order</button> để tạo đơn hàng mới
- Sau khi tạo đơn hàng mới có thểm bấm <button style="background-color: orange ; color:black" >Back Order List</button> để quay lại trang Order List

### 3. Chức năng xóa đơn hàng

#### 📰 Thao tác xóa đơn hàng:
- Bấm nút <button style="background-color: red ; color:white" >Delete</button> để xóa một đơn hàng muốn
- Một cửa số confirm sẽ hiện lên để chắc chắn rằng bạn muốn xóa đơn hàng

![DeleteOrder](imgMarkdown/DeleteOrder.PNG)

- bấm <button style="background-color: red ; color:white" >OK</button> để xác nhận xóa đơn hàng.
- bấm <button style="background-color: black ; color:white" >Cancel</button> để không xóa đơn hàng nữa. 

## 💻 Công nghệ đã dùng
+ Front-End:
    - 1. [Boostrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
    - 2. Javascript
+ Back-end:
+ KIT:
+ DB: